FactoryBot.define do
  factory :user do
    sequence(:first_name) { |n| "John #{n}" }
  end
end