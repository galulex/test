Rails.application.routes.draw do
  root 'reservations#new'

  resources :reservations
  resources :cars
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
